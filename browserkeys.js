var browsers = require('./src/browsers.js').browsers;
var keys = {};
var all = [];

var returnArray = function (obj) {
  if(obj.slice) {
    return obj;
  } else {
    return Object.keys(obj).reduce(function(arr, key) {
      if(obj[key].slice) {
        return arr.concat(obj[key]);
      } else {
        return arr.concat(returnArray(obj[key]));
      }
    }, []);
  }
};

console.log(Object.keys(returnArray(browsers).reduce(function(obj, browser) {
  Object.keys(browser).forEach(function(key) {
    obj[key] = true;
  });
  return obj;
}, {})));
