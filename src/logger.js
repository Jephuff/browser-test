var DEBUG_LEVEL = 4; //0 = none, 1 = error, 2 = warn, 3 = status, 4 = all
var logger = function(level) {
  if(level <= DEBUG_LEVEL && level > 0) {
    var logFunc;
    switch(level) {
      case 1:
        logFunc = console.error;
        break;
      case 2:
        logFunc = console.warn;
        break;
      default:
        logFunc = console.log;
        break;
    }
    logFunc.apply(console, Array.prototype.slice.call(arguments, 1));
  }
};

logger.ERROR = 1;
logger.WARN = 2;
logger.STAT = 3;
logger.ALL = 4;

module.exports = logger;
