var wd = require('wd');
var logger = require('./logger.js');

var username;
var accessKey;

module.exports = {
  authenticate: function(){
    // checking browserstack credential
    username = process.env.BROWSERSTACK_USERNAME;
    accessKey = process.env.BROWSERSTACK_ACCESS_KEY;
    if(!username || !accessKey){
      logger(1, '\nPlease configure your BROWSERSTACK credential:\n\nexport BROWSERSTACK_USERNAME=<BROWSERSTACK_USERNAME>\nexport BROWSERSTACK_ACCESS_KEY=<BROWSERSTACK_ACCESS_KEY>\n\n');
      throw new Error("Missing browserstack credentials");
    }
  },

  getNewBrowser: function(system){
    var systemClone = Object.keys(system)
      .reduce(function(clone, key) {
        clone[key] = system[key];
        return clone;
      }, {});

    systemClone['browserstack.user'] = username;
    systemClone['browserstack.key'] = accessKey;
    return wd.promiseChainRemote("http://hub.browserstack.com/wd/hub", 80)
      .init(systemClone);
  }
};
