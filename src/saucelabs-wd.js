var wd = require('wd');
var logger = require('./logger.js');

var username;
var accessKey;

module.exports = {
  authenticate: function(){
    // checking sauce credential
    username = process.env.SAUCE_USERNAME;
    accessKey = process.env.SAUCE_ACCESS_KEY;
    if(!username || !accessKey){
      logger(2, '\nPlease configure your sauce credential:\n\nexport SAUCE_USERNAME=<SAUCE_USERNAME>\nexport SAUCE_ACCESS_KEY=<SAUCE_ACCESS_KEY>\n\n');
      throw new Error("Missing sauce credentials");
    }
  },

  getNewBrowser: function(system){
    return wd.promiseChainRemote("ondemand.saucelabs.com", 80, username, accessKey)
      .init(system);
  }
};
