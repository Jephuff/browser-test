'use strict';
var fs = require('fs');
var _ = require('lodash');
var Promise = require('bluebird');
var compare = require('./imagemagick-compare.js');
var logger = require('./logger.js');

var current;
var systemErrorLogFile = 'system-fail.log';
var concurrency = 1; // the maximum number of concurrent tests to run.

function test(wd, system, urls, rootDir, scrollCommand){
  logger(1, '\ntesting system: ', system);

  var curPath = rootDir + current + '/';
  var lastPath = rootDir + (current - 1) + '/';

<<<<<<< HEAD:test.js
function test(wd, system){
  if(debugLevel >= 4) console.log('\ntesting system: ', system);

  var curPath = screenShotRootDir + current + '/';
  var lastPath = screenShotRootDir + (current - 1) + '/';



  var systemSlug = Object.keys(system).reduce(function(slug, key){
    return slug + '-' + system[key];
  }, '');
  
  // var systemSlug = system.deviceName || system.device ? (system.deviceName || system.device) + '-' + (system['device-orientation'] || '') : '';
  // systemSlug += system.platform + '-' + system.browserName + (system.version || '') + '-' + system.screenResolution;
=======
  var systemSlug = [
    system.deviceName || system.device,
    system['device-orientation'],
    system.platform || system.os,
    system.os_version,
    system.browserName || system.browser,
    system.version || system.browser_version,
    system.screenResolution,
    system.noScroll
  ].filter(function(part) { return part; }).join('-');
>>>>>>> 8c63fdb68c5112abd6542cbf54a9d3d9bbc5669c:src/test.js

  var browser = wd.getNewBrowser(system);
  var results = [];

  var chain = urls.reduce(function(chain, url){
    var windowHeight;
    var contentHeight;
    logger(1, 'testing url: ', url);
    var filename = (url.slug + '-' + systemSlug + '.png').replace(' ', '');

    chain = chain
      .get(url.url)
      .then(function(){
        logger(4, 'got url: ', url.url);
        if(!system.noScroll){
          return _.cloneDeep(browser)
          .elementByCssSelector('body')
          .getSize()
          .then(function(size){
            contentHeight = size.height;
            return _.cloneDeep(browser)
              .getWindowSize(function(err, size){
                windowHeight = size.height;
              })
              .catch(function(){
                windowHeight = 600;
              });
          });
        }
      });
    return chain
      .then(function () {
        var numScrolls = system.noScrolll || !windowHeight || !contentHeight ? 1 : Math.ceil(contentHeight / windowHeight);
        var secondChain = _.cloneDeep(browser);
        var urlResults = [];

        var scrollPage = function (scrollHeight) {
          return secondChain
            .execute(scrollCommand(0, scrollHeight))
            .saveScreenshot(curPath + 'scroll' + scrollHeight + filename)
            .then(function(){
              return compare(curPath + 'scroll' + scrollHeight + filename, lastPath + 'scroll' + scrollHeight + filename);
            })
            .then(function(result){
              result.system = system;
              result.lastPath = lastPath;
              result.curPath = curPath;
              result.filename = 'scroll' + scrollHeight + filename;
              result.urlSlug = url.slug;
              urlResults.push(result);
            });
        };

        for(var i = 0; i < numScrolls; i += 1){
            var scrollHeight = windowHeight ? windowHeight * i : 0;
            secondChain = scrollPage(scrollHeight);
        }

        results.push(urlResults);

        return secondChain;
      });
  }, _.cloneDeep(browser));

  return chain
    .then(function() {
      return browser.quit(function (err) {
        logger(4, 'browser quit ', err);
      });
    })
    .catch(function(err){
      logger(2, 'catch system failed: ', err);
      fs.writeFile(systemErrorLogFile, '\n-------------------------\nsystem: \n' + JSON.stringify(system) + '\n\n\n err:\n' + err , {flag: 'a'}, function (err) {});
    })
    .then(function(){ return results; });
}

function validateOptions(options){
  options = options || {};
  concurrency = options.concurrency || concurrency;
  current = options.current;
  systemErrorLogFile = options.systemErrorLogFile || systemErrorLogFile;
}

module.exports = function(wd, options, pages){
  validateOptions(options);

  wd.authenticate();
  var browsers = options.browsers;

  // queue test
  var progress = 0;
  var started = 0;

  return Promise.map(browsers, function(browser){
    started += 1;
    logger(1, 'starting test ' + started + ' of ' + browsers.total);
    return test(wd, browser, pages, options.rootDir, options.scrollCommand)
      .then(function(results){
        progress += 1;
        logger(1, 'completed ' + progress + ' of ' + browsers.total);
        return results;
      });
  }, {concurrency: concurrency});
};
