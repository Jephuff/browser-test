var fs = require('fs');
var logger = require('./logger.js');

var current;
var screenShotRootDir = './screenshots/';
var openInBrowser = false;
var equalityRatings = {
  good: 0.001, // <= to this will be considered equal
  warn: 0.01 // <= to this will have warning, greater then is considered an error.
};


module.exports = function (options){
  screenShotRootDir = options.screenShotRootDir || screenShotRootDir;
  equalityRatings = options.equalityRatings || equalityRatings;
  current = options.current || -1;

  logger(4, 'making index and report html');
  var minutes = (options.duration / 1000 / 60) << 0;
  var seconds = (options.duration / 1000) % 60;
  logger(4, 'duration ' + minutes + ':' + seconds);

  var index = '';
  var regression = '';

  var table = '<table><tr><th></th>';
  options.responses[0].forEach(function(url){
    table += '<th colspan=' + url.length + '>' + url[0].urlSlug + '</th>';
  });
  table += '</tr>';
  // var table = '<table><tr><th></th>';
  // options.pages.forEach(function(url){
  //   table += '<th>' + url.slug + '</th>';
  // });
  // table += '</tr>';
  var maxScrolls = [];
  options.responses.forEach(function(browser) {
    browser.forEach(function(scrolls, idx) {
      maxScrolls[idx] = maxScrolls[idx] || 0;
      maxScrolls[idx] = Math.max(maxScrolls[idx], scrolls.length);
    });
  });

  options.responses.forEach(function (browser){
    if(browser && browser[0] && browser[0][0] && browser[0][0].system){
      var details = 'System: <br>';
      var indexImages = '';
      var systemDetails = '';
      var rating = '';

      Object.keys(browser[0][0].system).forEach(function(key){
        systemDetails += key + ': ' + browser[0][0].system[key] + '<br>';
      });

      details += systemDetails + '<br>';
      table += '<tr><td>' + systemDetails + '</td>';

      browser.forEach(function(pageScrolls, idx){
        var i;
        indexImages += '<div class="page-scrolling">';
        for(i = 0; i < pageScrolls.length; i += 1){
          if(pageScrolls[i].equality === null){
            rating = 'error';
          } else if(pageScrolls[i].equality <= equalityRatings.good){
            rating = 'good';
          } else if(pageScrolls[i].equality <= equalityRatings.warn){
            rating = 'warn';
          } else if(pageScrolls[i].equality > equalityRatings.warn){
            rating = 'error';
          }
          table += '<td class="' + rating + '""><a href="' + '../compare.html?last=' + pageScrolls[i].lastPath + pageScrolls[i].filename + '&cur=' + pageScrolls[i].curPath + pageScrolls[i].filename + '">'+ (pageScrolls[i].equality === null ? 'size' : pageScrolls[i].equality) + '</a></td>';
          var smallerImage = pageScrolls[i].filename.replace('.png', '-460w.png')
          indexImages += '<div class="page"><img onerror="imageLoadFailed(this)" src="' + smallerImage + '"/></div>';
          regression += '<div class="browser"><div class="info">' + details + '</div><div class="page"><img onerror="imageLoadFailed(this)" src="' + smallerImage + '"/></div><div class="page"><img onerror="imageLoadFailed(this)" src="../' + (current - 1) + '/' + smallerImage + '"/></div><div class="page"><img onerror="imageLoadFailed(this)" src="../' + (current - 2) + '/' + smallerImage + '"/></div></div>';
        }
        for(; i < maxScrolls[idx]; i += 1) {
          table += '<td></td>';
        }
        indexImages += '</div>';
      });


      
      table += '</tr>';
      index += '<div class="browser"><div class="info">' + details + '</div>' + indexImages + '</div>';
    } else {
      logger(3, 'system info missing: ', browser);
    }
  });
  table += '</table>';

  var paths = [
    makeHTMLFile('report.html', table),
    makeHTMLFile('index.html', index),
    makeHTMLFile('regression.html', regression, regressionScript())
  ];
  return paths;
};


function regressionScript(){
  var scripts = '<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>';
  scripts += '<script>$(".browser").on("click", function (el) {var el = $(this); el.hasClass("full") ? el.removeClass("full") : el.addClass("full")});</script>';
  return scripts;
}

function makeHTMLFile(filename, body, script) {
  script = script || '';
  logger(4, 'saving ' + filename);
  var css = '../main.css';
  var fallbackImageSCript = '<script>function imageLoadFailed(img){ if(img.src.indexOf("-460w") >= 0) img.src = img.src.replace("-460w", "");}</script>';
  var html =  '<!DOCTYPE html><head><link rel="stylesheet" type="text/css" href="' + 
              css + '">' + fallbackImageSCript + '</head><body>' + body + script + '</body>';
  var path = current + '/' + filename;
  var savePath = screenShotRootDir + path;
  var output = fs.createWriteStream(savePath);
  output.once('open', function(fd) {
    output.end(html);
    if(openInBrowser) openBrowser(savePath);
  });

  return path;
}

function openBrowser(path){
  logger(4, 'opening in chrome');
  require('child_process').exec('google-chrome ' + path , function(err) {});
}