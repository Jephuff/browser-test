'use strict';
var browsers = {
  saucelabs: {
    ie: [
      // windows xp
      {
        "browserName": "internet explorer",
        "platform": "Windows XP",
        "version": "8.0",
        "record-video": true,
        "record-screenshots": true
      },
      // // windows 7
      // {
      //   "browserName": "internet explorer",
      //   "platform": "Windows 7",
      //   "version": "8.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      {
        "browserName": "internet explorer",
        "platform": "Windows 7",
        "version": "9.0",
        "record-video": true,
        "record-screenshots": true
      },
      {
        "browserName": "internet explorer",
        "platform": "Windows 7",
        "version": "10.0",
        "record-video": true,
        "record-screenshots": true
      },
      // {
      //   "browserName": "internet explorer",
      //   "platform": "Windows 7",
      //   "version": "11.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 8
      // {
      //   "browserName": "internet explorer",
      //   "platform": "Windows 8",
      //   "version": "10.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 8.1
      {
        "browserName": "internet explorer",
        "platform": "Windows 8.1",
        "version": "11.0",
        "record-video": true,
        "record-screenshots": true
      }
    ],
    safari: [
      {
        "browserName": "safari",
        "platform": "Windows 7",
        "version": "5.1",
        "record-video": true,
        "record-screenshots": true
      },
      // OS X Snow Leopard(10.6)
      {
        "browserName": "safari",
        "platform": "OS X 10.6",
        "version": "5.1",
        "record-video": true,
        "record-screenshots": true
      },
      // OS X Mountain Lion(10.8)
      {
        "browserName": "safari",
        "platform": "OS X 10.8",
        "version": "6.0",
        "record-video": true,
        "record-screenshots": true
      },
      // OS X Mavericks(10.9)
      {
        "browserName": "safari",
        "platform": "OS X 10.9",
        "version": "7.0",
        "record-video": true,
        "record-screenshots": true
      },
      // OS X Yosemite(10.10)
      {
        "browserName": "safari",
        "platform": "OS X 10.10",
        "version": "8.0",
        "record-video": true,
        "record-screenshots": true
      }
    ],
    firefox: [
      // {
      //   "browserName": "firefox",
      //   "platform": "Windows XP",
      //   "version": "37.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "firefox",
      //   "platform": "Windows XP",
      //   "version": "35.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 7
      {
        "browserName": "firefox",
        "platform": "Windows 7",
        "version": "37.0",
        "record-video": true,
        "record-screenshots": true,
        "noScroll": true
      },
      {
        "browserName": "firefox",
        "platform": "Windows 7",
        "version": "35.0",
        "record-video": true,
        "record-screenshots": true,
        "noScroll": true
      },
      // windows 8
      // {
      //   "browserName": "firefox",
      //   "platform": "Windows 8",
      //   "version": "37.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "firefox",
      //   "platform": "Windows 8",
      //   "version": "35.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 8.1
      {
        "browserName": "firefox",
        "platform": "Windows 8.1",
        "version": "37.0",
        "record-video": true,
        "record-screenshots": true,
        "noScroll": true
      },
      {
        "browserName": "firefox",
        "platform": "Windows 8.1",
        "version": "35.0",
        "record-video": true,
        "record-screenshots": true,
        "noScroll": true
      },
      // OS X Snow Leopard(10.6)
      // {
      //   "browserName": "firefox",
      //   "platform": "OS X 10.6",
      //   "version": "37.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "firefox",
      //   "platform": "OS X 10.6",
      //   "version": "35.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // OS X Mavericks(10.9)
      // {
      //   "browserName": "firefox",
      //   "platform": "OS X 10.9",
      //   "version": "37.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "firefox",
      //   "platform": "OS X 10.9",
      //   "version": "35.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // OS X Yosemite(10.10)
      {
        "browserName": "firefox",
        "platform": "OS X 10.10",
        "version": "37.0",
        "record-video": true,
        "record-screenshots": true,
        "noScroll": true
      },
      {
        "browserName": "firefox",
        "platform": "OS X 10.10",
        "version": "35.0",
        "record-video": true,
        "record-screenshots": true,
        "noScroll": true
      }
    ],
    chrome: [
      // {
      //   "browserName": "chrome",
      //   "platform": "Windows XP",
      //   "version": "42.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "chrome",
      //   "platform": "Windows XP",
      //   "version": "40.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 7
      // {
      //   "browserName": "chrome",
      //   "platform": "Windows 7",
      //   "version": "42.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "chrome",
      //   "platform": "Windows 7",
      //   "version": "40.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 8
      // {
      //   "browserName": "chrome",
      //   "platform": "Windows 8",
      //   "version": "42.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "chrome",
      //   "platform": "Windows 8",
      //   "version": "40.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // windows 8.1
      {
        "browserName": "chrome",
        "platform": "Windows 8.1",
        "version": "42.0",
        "record-video": true,
        "record-screenshots": true
      },
      {
        "browserName": "chrome",
        "platform": "Windows 8.1",
        "version": "40.0",
        "record-video": true,
        "record-screenshots": true
      },
      // OS X Snow Leopard(10.6)
      // {
      //   "browserName": "chrome",
      //   "platform": "OS X 10.6",
      //   "version": "42.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "chrome",
      //   "platform": "OS X 10.6",
      //   "version": "40.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // OS X Mountain Lion(10.8)
      // {
      //   "browserName": "chrome",
      //   "platform": "OS X 10.8",
      //   "version": "42.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "chrome",
      //   "platform": "OS X 10.8",
      //   "version": "40.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // OS X Mavericks(10.9)
      // {
      //   "browserName": "chrome",
      //   "platform": "OS X 10.9",
      //   "version": "42.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // {
      //   "browserName": "chrome",
      //   "platform": "OS X 10.9",
      //   "version": "40.0",
      //   "record-video": true,
      //   "record-screenshots": true
      // },
      // OS X Yosemite(10.10)
      {
        "browserName": "chrome",
        "platform": "OS X 10.10",
        "version": "42.0",
        "record-video": true,
        "record-screenshots": true
      },
      {
        "browserName": "chrome",
        "platform": "OS X 10.10",
        "version": "40.0",
        "record-video": true,
        "record-screenshots": true
      }
    ],
    iphone: [
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "4.3",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "4.3",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "landscape"
      },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "5.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "portrait"
      // },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "5.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "landscape"
      // },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "5.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "portrait",
        "noScroll": true
      },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "5.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "landscape",
        "noScroll": true
      },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "6.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "portrait"
      // },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "6.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "landscape"
      // },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "6.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "portrait",
        "noScroll": true
      },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "6.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "landscape",
        "noScroll": true
      },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "7.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "portrait"
      // },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "7.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "landscape"
      // },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "7.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "7.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "landscape"
      },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "8.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "portrait"
      // },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "8.0",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "landscape"
      // },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "8.1",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "portrait"
      // },
      // {
      //   "browserName": "iphone",
      //   "platform": "OS X 10.10",
      //   "version": "8.1",
      //   "record-video": true,
      //   "record-screenshots": true,
      //   "deviceName": "iPhone Simulator",
      //   "device-orientation": "landscape"
      // },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "8.2",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "iphone",
        "platform": "OS X 10.10",
        "version": "8.2",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "iPhone Simulator",
        "device-orientation": "landscape"
      }
    ],
    android: [
      {
        "browserName": "android",
        "platform": "Linux",
        "version": "4.0",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "Android Emulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "android",
        "platform": "Linux",
        "version": "4.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "Android Emulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "android",
        "platform": "Linux",
        "version": "4.2",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "Android Emulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "android",
        "platform": "Linux",
        "version": "4.3",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "Android Emulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "android",
        "platform": "Linux",
        "version": "4.4",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "Android Emulator",
        "device-orientation": "portrait"
      },
      {
        "browserName": "android",
        "platform": "Linux",
        "version": "5.1",
        "record-video": true,
        "record-screenshots": true,
        "deviceName": "Android Emulator",
        "device-orientation": "portrait"
      }
    ]
  },
  browserstack: {
    ie: [
      {
        "browser": "IE",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "6.0"
      },
      {
        "browser": "IE",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "7.0"
      },
      {
        "browser": "IE",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "8.0",
        "noScroll": true
      },
      {
        "browser": "IE",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "9.0"
      },
      {
        "browser": "IE",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "10.0"
      },
      {
        "browser": "IE",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "11.0"
      }
    ],
    safari: [
      // {
      //   "browser": "safari",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "5.1"
      // },
      {
        "browser": "safari",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "5.1"
      },
      // {
      //   "browser": "safari",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "5.1"
      // },
      // {
      //   "browser": "safari",
      //   "os_version": "8.1",
      //   "os": "Windows",
      //   "browser_version": "5.1"
      // },
      {
        "browser": "safari",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "5.1"
      },
      {
        "browser": "safari",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "6.0"
      },
      {
        "browser": "safari",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "6.1"
      },
      {
        "browser": "safari",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "7.0"
      },
      {
        "browser": "safari",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "8.0"
      },
    ],
    firefox: [
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "4.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "5.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "6.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "7.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "8.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "9.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "10.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "11.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "12.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "13.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "37.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "3.6"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "4.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "5.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "6.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "7.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "8.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "9.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "10.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "11.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "12.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "13.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "37.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "3.6"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "4.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "5.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "6.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "7.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "8.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "9.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "10.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "11.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "12.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "13.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "37.0"
      // },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "3.6"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "4.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "5.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "6.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "7.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "8.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "9.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "10.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "11.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "12.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "13.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "14.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "15.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "16.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "17.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "18.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "19.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "20.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "21.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "22.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "23.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "24.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "25.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "26.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "27.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "28.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "29.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "30.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "31.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "32.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "33.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "34.0"
      },
      {
        "browser": "firefox",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "35.0",
        "noScroll": true
      },
      // {
      //   "browser": "firefox",
      //   "os_version": "Yosemite",
      //   "os": "OS X",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Yosemite",
      //   "os": "OS X",
      //   "browser_version": "37.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "3.6"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "4.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "5.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "6.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "7.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "8.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "9.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "10.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "11.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "12.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "13.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "37.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "37.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "3.6"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "4.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "5.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "6.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "7.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "8.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "9.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "10.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "11.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "12.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "13.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "37.0"
      // },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "16.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "17.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "18.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "19.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "20.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "21.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "22.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "23.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "24.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "25.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "26.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "27.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "28.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "29.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "30.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "31.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "32.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "33.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "34.0"
      },
      {
        "browser": "firefox",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "35.0",
        "noScroll": true
      },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "3.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "3.6"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "4.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "5.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "6.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "7.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "8.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "9.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "10.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "11.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "12.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "13.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "firefox",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "37.0"
      // },
    ],
    chrome: [
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "14.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "16.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "17.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "18.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "19.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "20.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "21.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "39.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Snow Leopard",
      //   "os": "OS X",
      //   "browser_version": "42.0"
      // },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "14.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "16.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "17.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "18.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "19.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "20.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "21.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "39.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Mavericks",
      //   "os": "OS X",
      //   "browser_version": "42.0"
      // },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "14.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "16.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "17.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "18.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "19.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "20.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "21.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "39.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Mountain Lion",
      //   "os": "OS X",
      //   "browser_version": "42.0"
      // },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "14.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "16.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "17.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "18.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "19.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "20.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "21.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "39.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "40.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "41.0"
      },
      {
        "browser": "chrome",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "42.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "14.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "16.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "17.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "18.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "19.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "20.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "21.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "39.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "Lion",
      //   "os": "OS X",
      //   "browser_version": "42.0"
      // },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "39.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "8",
      //   "os": "Windows",
      //   "browser_version": "42.0"
      // },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "14.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "15.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "16.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "17.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "18.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "19.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "20.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "21.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "39.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "7",
      //   "os": "Windows",
      //   "browser_version": "42.0"
      // },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "22.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "23.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "24.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "25.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "26.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "27.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "28.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "29.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "30.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "31.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "32.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "33.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "34.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "35.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "36.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "37.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "38.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "39.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "40.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "41.0"
      },
      {
        "browser": "chrome",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "42.0"
      },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "14.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "15.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "16.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "17.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "18.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "19.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "20.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "21.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "22.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "23.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "24.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "25.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "26.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "27.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "28.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "29.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "30.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "31.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "32.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "33.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "34.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "35.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "36.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "37.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "38.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "39.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "40.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "41.0"
      // },
      // {
      //   "browser": "chrome",
      //   "os_version": "XP",
      //   "os": "Windows",
      //   "browser_version": "42.0"
      // },
    ],
    iphone: [
      {
        "browser": "Mobile Safari",
        "os_version": "7.0",
        "os": "ios",
        "device": "iPhone 5S"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "5.0",
        "os": "ios",
        "device": "iPad 2 (5.0)"
      },

      {
        "browser": "Mobile Safari",
        "os_version": "7.0",
        "os": "ios",
        "device": "iPad Mini"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "7.0",
        "os": "ios",
        "device": "iPad 4th"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "6.0",
        "os": "ios",
        "device": "iPhone 4S (6.0)"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "6.0",
        "os": "ios",
        "device": "iPhone 5"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "6.0",
        "os": "ios",
        "device": "iPad 3rd (6.0)"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "5.1",
        "os": "ios",
        "device": "iPhone 4S"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "5.1",
        "os": "ios",
        "device": "iPad 3rd"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "8.3",
        "os": "ios",
        "device": "iPhone 6"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "8.3",
        "os": "ios",
        "device": "iPhone 6 Plus"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "8.3",
        "os": "ios",
        "device": "iPad Mini 2"
      },
      {
        "browser": "Mobile Safari",
        "os_version": "8.3",
        "os": "ios",
        "device": "iPad Air"
      }
    ],
    android: [
      {
       'browserName' : 'android',
       'platform' : 'ANDROID',
       'device' : 'Samsung Galaxy Note 2',
        "noScroll": true
      },
      {
       'browserName' : 'android',
       'platform' : 'ANDROID',
       'device' : 'Google Nexus 4',
        "noScroll": true
      },
      {
       'browserName' : 'android',
       'platform' : 'ANDROID',
       'device' : 'Samsung Galaxy Note 3',
        "noScroll": true
      },
      {
       'browserName' : 'android',
       'platform' : 'ANDROID',
       'device' : 'Samsung Galaxy S5',
        "noScroll": true
      }
    ],
    opera: [
      {
        "browser": "opera",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "11.6"
      },
      {
        "browser": "opera",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "12.0"
      },
      {
        "browser": "opera",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "12.12"
      },
      {
        "browser": "opera",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "Snow Leopard",
        "os": "OS X",
        "browser_version": "12.15"
      },



      {
        "browser": "opera",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "11.6"
      },
      {
        "browser": "opera",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "12.0"
      },
      {
        "browser": "opera",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "12.12"
      },
      {
        "browser": "opera",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "Mavericks",
        "os": "OS X",
        "browser_version": "12.15"
      },



      {
        "browser": "opera",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "11.6"
      },
      {
        "browser": "opera",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "12.0"
      },
      {
        "browser": "opera",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "12.12"
      },
      {
        "browser": "opera",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "Mountain Lion",
        "os": "OS X",
        "browser_version": "12.15"
      },



      {
        "browser": "opera",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "12.12"
      },
      {
        "browser": "opera",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "Yosemite",
        "os": "OS X",
        "browser_version": "12.15"
      },



      {
        "browser": "opera",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "11.6"
      },
      {
        "browser": "opera",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "12.0"
      },
      {
        "browser": "opera",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "12.12"
      },
      {
        "browser": "opera",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "Lion",
        "os": "OS X",
        "browser_version": "12.15"
      },



      {
        "browser": "opera",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "12.0"
      },
      {
        "browser": "opera",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "12.10"
      },
      {
        "browser": "opera",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "12.15"
      },
      {
        "browser": "opera",
        "os_version": "8",
        "os": "Windows",
        "browser_version": "12.16"
      },




      {
        "browser": "opera",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "11.6"
      },
      {
        "browser": "opera",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "12.10"
      },
      {
        "browser": "opera",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "12.15"
      },
      {
        "browser": "opera",
        "os_version": "7",
        "os": "Windows",
        "browser_version": "12.16"
      },



      {
        "browser": "opera",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "12.0"
      },
      {
        "browser": "opera",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "12.10"
      },
      {
        "browser": "opera",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "12.15"
      },
      {
        "browser": "opera",
        "os_version": "8.1",
        "os": "Windows",
        "browser_version": "12.16"
      },
      {
        "browser": "opera",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "11.6"
      },
      {
        "browser": "opera",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "12.10"
      },
      {
        "browser": "opera",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "12.14"
      },
      {
        "browser": "opera",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "12.15"
      },
      {
        "browser": "opera",
        "os_version": "XP",
        "os": "Windows",
        "browser_version": "12.16"
      },
    ]
  }
};
//'noScroll': true

function validateOptions(obj){
  obj = obj || {};
  obj.remote = obj.remote || 'saucelabs';
  obj.include = obj.include || [];
  obj.exclude = obj.exclude || [];
  return obj;
}

module.exports = function (options) {
  options = validateOptions(options);
  var b = browsers[options.remote];
  var selectedBrowsers = [];

  if(options.include.length > 0){
    options.include.forEach(function(browser){
        selectedBrowsers = selectedBrowsers.concat(b[browser.browser].filter(function(item){
          return (!browser.versions ||  browser.versions.indexOf(item.version || item['browser_version']) >= 0) ? true : false;
        }));
    });
  } else {
    Object.keys(b).forEach(function(key){
      if(options.exclude){
        var check = options.exclude.reduce(function(a, b){ return (a.browser === key) ? a : b; });
        if(check.browser === key){
          if(check.versions && check.versions.length >= 1){
            selectedBrowsers = selectedBrowsers.concat(b[key].filter(function(item){
              return (check.versions.indexOf(item.version) >= 0) ? false : true;
            }));
          }
        } else {
          selectedBrowsers = selectedBrowsers.concat(b[key]);  
        }
      } else {
        selectedBrowsers = selectedBrowsers.concat(b[key]);
      }
    });
  }

  selectedBrowsers.total = selectedBrowsers.length;
  return selectedBrowsers;
};

module.exports.browsers = browsers;
