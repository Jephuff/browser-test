var logger = require('./logger.js');

var apiKey;
var domain;

function emailBody(reports, reportDomain) {
  return reports.map(function(path){
    if(reportDomain) {
      var link = reportDomain + path;
      return '<a href="' + link + '">' + link + '</a>';
    } else {
      return path;
    }
  }).join('<br>');
}

module.exports = {
  checkCreds: function() {
    apiKey = process.env.MAILGUN_API_KEY;
    domain = process.env.MAILGUN_DOMAIN;
    if(!apiKey || !domain){
      logger(2, '\nPlease configure your MailGun credential:\n\nexport MAILGUN_API_KEY=<mailgun api key>\nexport MAILGUN_DOMAIN=<mailgun domain>\n\n');
      throw new Error("Missing mailgun credentials");
    }
  },

  send: function(email, reports, reportDomain) {
    this.checkCreds();

    var mailgun = require('mailgun-js')({apiKey: apiKey, domain: domain});

    if(reportDomain && reportDomain[reportDomain.length - 1] !== '/') {
      reportDomain = reportDomain + '/';
    }
    var data = {
      from: 'Screenshot Automation <me@samples.mailgun.org>',
      to: email,
      subject: 'browser test reports',
      html: emailBody(reports, reportDomain)
    };

    mailgun.messages().send(data)
      .then(function (body) {
        logger(4, body);
      }).catch(function(err){
        logger(1, err);
      });
  }
};
