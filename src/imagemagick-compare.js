var fs = require('fs');
var Promise = require('bluebird');
var gm = require('gm').subClass({imageMagick: true});
var logger = require('./logger.js');

module.exports = function (filePath1, filePath2){
  var result = { equality: null };

  return new Promise(function (resolve, reject) {
    if(fs.existsSync(filePath2)){
      gm().compare(filePath1, filePath2, function (err, isEqual, equality, raw, file1, file2){
        if (err){
          logger(3, 'failed to compare screenshots: ', err);
          result.err = err;
        } else {
          logger(4, 'successfuly compared screenshots', file1, ' and ', file2);
          result.equality = equality;
        }
        resolve(result);
      });
    } else {
      logger(3, filePath2, ' does not exist');
      resolve(result);
    }
  });
};
