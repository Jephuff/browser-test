var test = require('./test.js');
var fs = require('fs');
var Promise = require('bluebird');
var report = require('./report.js');
var gm = require('gm').subClass({imageMagick: true});
var mkdirp = require('mkdirp');
var expandTilde = require('expand-tilde');
var logger = require('./logger.js');
var alert = require('./alert.js');
var drivers = {
  saucelabs: require('./saucelabs-wd.js'),
  browserstack: require('./browserstack-wd.js')
};
// defaults
var DEFAULT_ROOT_DIR = '~/screenshots/';
var DEFAULT_CONCURRENCY = 1;
var DEFAULT_SCROLL_COMMAND = function(x, y) { return 'window.scrollTo && window.scrollTo(' + x + ',' + y + ')'; };

function startTest(opts) {
  return test(opts.driver, {
    current: opts.current,
    concurrency: opts.config.concurrency || DEFAULT_CONCURRENCY,
    rootDir: opts.rootDir,
    browsers: opts.config.browsers,
    scrollCommand: opts.scrollCommand
  }, opts.pages);
}

module.exports = function(opts, callback) {
  return new Promise(function(resolve, reject) {
    if(opts.email) {
      alert.checkCreds();
    }
    var error;
    if(!opts || typeof opts !== 'object') {
      error = new Error('first parameter must be an object');
      reject(error);
      if(callback) {
        callback(error);
      }
    } else if(!opts.pages || opts.pages.length === 0) {
      error = new Error('no pages specified');
      reject(error);
      if(callback) {
        callback(error);
      }
    } else if(!opts.targets || opts.targets.length < 1) {
      error = new Error('no targets');
      reject(error);
      if(callback) {
        callback(error);
      }
    } else {
      // setup root directory
      var rootDir = expandTilde(opts.rootDir || DEFAULT_ROOT_DIR);
      rootDir = rootDir[rootDir.length - 1] === '/' ? rootDir : rootDir + '/';
      mkdirp.sync(rootDir);
      if(!fs.existsSync(rootDir + 'main.css')) {
        fs.createReadStream(__dirname + '/main.css').pipe(fs.createWriteStream(rootDir + 'main.css'));
      }

      if(!fs.existsSync(rootDir + 'compare.html')) {
        fs.createReadStream(__dirname + '/compare.html').pipe(fs.createWriteStream(rootDir + 'compare.html'));
      }

      // get current iteration and make the directory
      var current = opts.current || (fs.existsSync(rootDir + '.current') ? parseInt(fs.readFileSync(rootDir + '.current', 'utf8')) : 1);
      fs.writeFileSync(rootDir + '.current', current += 1, 'utf8');
      mkdirp.sync(rootDir + current);

      // setup vars for tests
      var startTime = new Date();

      // create tests
      var tests = opts.targets.map(function(target) {
        var driver = target.driver || drivers[target.target];
        return startTest({
          driver: driver,
          config: target.config,
          current: current,
          rootDir: rootDir,
          pages: opts.pages,
          scrollCommand: opts.scrollCommand || DEFAULT_SCROLL_COMMAND
        });
      });

      return Promise.all(tests)
        .then(function(results){
          // generate reports
          var reports = report({
            responses: results.reduce(function(a, b){ return a.concat(b); }),
            duration: new Date() - startTime,
            current: current,
            screenShotRootDir: rootDir,
            pages: opts.pages
          });

          // resize images
          var files = fs.readdirSync(rootDir + current);
          files.filter(function(file){ return file.indexOf('.png') >= 0; })
            .forEach(function(file){
              var path = rootDir + current + '/' + file;
              gm(path)
                .size(function(err, size){
                  var image = gm(path);

                  if(size.width > 460) image = image.resize(460);

                  image
                    .noProfile()
                    .write(path.replace('.png', '-460w.png'), logger.bind(undefined, 3, 'writing resized image: '));
                });
            });

          // send links
          if(opts.email) {
            alert.send(opts.email, reports, opts.reportDomain);
          }

          resolve(results);
          if(callback) {
            callback(results);
          }
        });
    }
  });
};
