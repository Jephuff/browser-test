var screenshots = require('../src/index.js');
var browsers = require('../src/browsers.js');

var exampleOpts = {
  rootDir: '/home/jeffrey/screenshots/',
  // current: 1, // override current iteration.
  // email: 'jeffrey@jam3.com',
  // reportDomain: 'file://Users/jeffrey/screenshots/',
  // scrollCommand: function(x, y) { return 'window.scrollTo && window.scrollTo(' + x + ',' + y + ')'; }, // override scroll function if needed.
  targets: [
    {
      target: 'browserstack', // if driver is not specified, the target will be used to look one up.
      // driver: require('../src/browserstack-wd.js')
      config: {
        concurrency: 1,
        browsers: browsers({
          remote: 'browserstack', // also could be 'saucelabs'
          include: [
            {
              browser: 'chrome',
              versions: ['42.0']
            }
          ]
        })
      }
    }
  ],
  pages: [
    {
      slug: 'home',
      url: 'http://events.fb.com/'
    },
    {
      slug: 'public-events',
      url: 'http://events.fb.com/public-events'
    }
  ]
};

screenshots(exampleOpts)
  .then(function() { console.log('done'); })
  .catch(function(err) { console.log(err); });
