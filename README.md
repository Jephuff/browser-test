browser-test

test websites in a large number of VMs and compare screenshots

TO SETUP
install imageMagic

Configure your sauce credential:

export SAUCE_USERNAME=<SAUCE_USERNAME>
export SAUCE_ACCESS_KEY=<SAUCE_ACCESS_KEY>

export BROWSERSTACK_USERNAME=<BROWSERSTACK_USERNAME>
export BROWSERSTACK_ACCESS_KEY=<BROWSERSTACK_ACCESS_KEY>

then npm install